#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

const int ledPin = 13; // Pin na kojem se nalazi LED dioda
const int buttonPin = 2; // Pin na kojem se nalazi tipkalo

volatile bool buttonPressed = false;

// Funkcija koja se poziva kada se detektira pritisak tipkala
void buttonInterrupt() {
  buttonPressed = true;
}

// Funkcija koja postavlja Arduino u sleep mode
void sleepNow() {
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Postavljanje željenog sleep moda
  sleep_enable(); // Omogućavanje sleep moda
  attachInterrupt(digitalPinToInterrupt(buttonPin), buttonInterrupt, LOW); // Postavljanje interrupta za tipkalo
  sleep_mode(); // Ulazak u sleep mode
  // Program se nastavlja izvršavati ovdje kada se Arduino probudi
  sleep_disable(); // Onemogućavanje sleep moda
  detachInterrupt(digitalPinToInterrupt(buttonPin)); // Detach interrupta za tipkalo
}

// Funkcija koja se poziva kada watchdog timer istekne
ISR(WDT_vect) {
  // Resetiranje watchdog timera
  wdt_disable(); // Onemogućavanje watchdog timera
}

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(buttonPin), buttonInterrupt, LOW); // Postavljanje interrupta za tipkalo

  // Postavljanje watchdog timera za 5 sekundi
  MCUSR &= ~(1<<WDRF); // Resetiranje watchdog timera
  WDTCSR |= (1<<WDCE) | (1<<WDE); // Omogućavanje promjene postavki watchdog timera
  WDTCSR = (1<<WDIE) | (1<<WDP2) | (1<<WDP0); // Postavljanje timer intervala na 5 sekundi
}

void loop() {

  // Primjer: Treptanje LED diode svakih 5 sekundi
  digitalWrite(ledPin, HIGH);
  delay(1000); // Ostanak u stanju uključenja
  digitalWrite(ledPin, LOW);
  delay(1000); // Ostanak u stanju isključenja

  // Provjerava je li tipkalo pritisnuto
  if (buttonPressed) {
    // Ako je pritisnuto tipkalo, odmah izlazi iz sleep moda
    buttonPressed = false;
    return;
  }
  // Ako nije pritisnuto tipkalo, postavlja Arduino u sleep mode
  sleepNow();
}
