#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);

#define S0 4
#define S1 5
#define S2 7
#define S3 6
#define sensorOut 8

int frequency = 0;
int color=0;

void setup() {
  mySoftwareSerial.begin(9600);
  Serial.begin(115200);
  
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);
  // Postavljanje frekvencijskog skaliranja
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);

  Serial.println();
  Serial.println(F("Initializing DFPlayer..."));

  // Korištenje softwareSerial za komunikaciju s MP3
  if (!myDFPlayer.begin(mySoftwareSerial)) {
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true);
  }
  Serial.println(F("DFPlayer Mini online."));

  // Postavljanje vrijednosti glasnoće (od 0 do 30)
  myDFPlayer.volume(30);
}


void loop() {
  
  color = readColor();
  delay(2000);  
  switch (color) {
    case 1:
    Serial.println("RED detected!");
    myDFPlayer.play(1);
    break;
    case 2:
    Serial.println("BLUE detected!");
    myDFPlayer.play(2);
    break;
    case 3:
    Serial.println("GREEN detected!");
    myDFPlayer.play(3);
    break;
    case 0:
    break;
  }
  color=0;
}


// Funkcija za čitanje boje
int readColor() {
  
  // Postavljanje fotodioda s crvenim filtrom za čitanje
  digitalWrite(S2, LOW);
  digitalWrite(S3, LOW);
  
  // Čitanje izlazne frekvencije
  frequency = pulseIn(sensorOut, LOW);
  int R = frequency;
  
  // Ispis vrijednosti na serijski monitor
  Serial.print("R= ");  // ispis imena
  Serial.print(frequency);  // ispis frekvencije crvene boje
  Serial.print("  ");
  delay(50);
  
  // Postavljanje fotodioda s zelenim filtrom za čitanje
  digitalWrite(S2, HIGH);
  digitalWrite(S3, HIGH);
  
  // Čitanje izlazne frekvencije
  frequency = pulseIn(sensorOut, LOW);
  int G = frequency;
  
  // Ispis vrijednosti na serijski monitor
  Serial.print("G= ");  // ispis imena
  Serial.print(frequency);  // ispis frekvencije zelene boje
  Serial.print("  ");
  delay(50);
  
  // Postavljanje fotodioda s plavim filtrom za čitanje
  digitalWrite(S2, LOW);
  digitalWrite(S3, HIGH);
  
  // Čitanje izlazne frekvencije
  frequency = pulseIn(sensorOut, LOW);
  int B = frequency;
  
  // Ispis vrijednosti na serijski monitor
  Serial.print("B= ");  // ispis imena
  Serial.print(frequency);  // ispis frekvencije plave boje
  Serial.println("  ");
  delay(50);
  
  if(R<180 & R>100 & G>R & B>R){
    color = 1; // Crveno
  }
  if(G>B & R>B & B<200 & B>100){
    color = 2; // Plavo
  }
  if(R>G & B>G & G<200 & G>100){
    color = 3; // Zeleno
  }
  return color;  
}
