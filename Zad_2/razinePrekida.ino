const int buttonPin1 = 2; // Pin za prvo tipkalo
const int buttonPin2 = 3; // Pin za drugo tipkalo
const int sensorPin = A0; // Pin za senzor
const int ledPin = 13; // Pin za LED diodu

volatile bool button1Pressed = false;
volatile bool button2Pressed = false;
volatile bool sensorActivated = false;

void setup() {
  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);
  pinMode(sensorPin, INPUT);
  pinMode(ledPin, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(buttonPin1), button1Interrupt, FALLING);
  attachInterrupt(digitalPinToInterrupt(buttonPin2), button2Interrupt, FALLING);
  // Priroritet se postavlja na vanjski prekid od senzora
  attachInterrupt(digitalPinToInterrupt(sensorPin), sensorInterrupt, FALLING);
}

void loop() {

  if (button1Pressed) {
    digitalWrite(ledPin, HIGH);
    delay(500);
    digitalWrite(ledPin, LOW);
    button1Pressed = false;
  }

  if (button2Pressed) {
    digitalWrite(ledPin, HIGH);
    delay(1000);
    digitalWrite(ledPin, LOW);
    button2Pressed = false;
  }

  if (sensorActivated) {
    // Obrada događaja kada se senzor aktivira
    // Na primjer, slanje poruke ili pokretanje određene funkcije
    sensorActivated = false;
  }
}

// Interrupt funkcija za pritisak prvog tipkala
void button1Interrupt() {
  button1Pressed = true;
}

// Interrupt funkcija za pritisak drugog tipkala
void button2Interrupt() {
  button2Pressed = true;
}

// Interrupt funkcija za aktivaciju senzora
void sensorInterrupt() {
  sensorActivated = true;
}
